---
title: Customize Open Swoole Grafana Dashboard
---

This document describes how to configure prometheus and grafana custom dashboard using PromQl Query Language.

## Prerequisites
 - Prometheus and Grafana
 - kube-prometheus-stack(helm chart)

## Installation

 - Helm repo add for prometheus and grafana

   ```sh
   $ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
   ```

- Edit values.yaml and install helm chart

  ```
  $ helm install kube-prometheus-stack prometheus-community/kube-prometheus-stack
  ```

## Configure Prometheus servicemonitor

Deploy service-monitor 

```yaml
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  labels:
    app.kubernetes.io/instance: entity-sit
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: entity
    app.kubernetes.io/version: 0.0.2
    argocd.argoproj.io/instance: entity-sit
    helm.sh/chart: entity-1.0.0
  name: entity
  namespace: sit
spec:
  endpoints:
    - honorLabels: true
      interval: 10s
      path: /metrics
      port: http
  namespaceSelector:
    matchNames:
      - sit
  selector:
    matchLabels:
      app.kubernetes.io/instance: entity-sit
      app.kubernetes.io/name: entity
```

## Install Grafana Open Swoole Dashboard

Open-Swoole Official Dashboard URL: [https://grafana.com/grafana/dashboards/15418-openswoole-dashboard/](https://grafana.com/grafana/dashboards/15418-openswoole-dashboard/)
  ```
  Dashboard setting 

  Dashboards >> Import >> dashboard ID or json file 
  ``` 

## Customize Open Swoole Dashboard
  ```
  Dashboard setting 

  Settings >> Variables >> add New Vaiable

  Name -> name of variable (example= app)
  Type -> Query (prometheus query PromQL)
  Label -> Display name
  Data source -> prometheus
  Query -> label_values(up, service)
  Regex -> /enforcement|entity|product|sael|tap-sin/
  ```

## Query (PromQL)
label_values() using prometheus default query variables

More template variables reference: [https://grafana.com/docs/grafana/latest/datasources/prometheus/template-variables/](https://grafana.com/docs/grafana/latest/datasources/prometheus/template-variables/)
